const responseMiddleware = (req, res, next) => {
    if (res.err) {
        const code = res.err.status || 400;
        res.status(code).send({
            error: true,
            message: res.err.message
        });
    } else {
        res.status(200).send(res.data);
    }

    next();
}

exports.responseMiddleware = responseMiddleware;