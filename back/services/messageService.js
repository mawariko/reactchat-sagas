const { MessageRepository } = require('../repositories/messageRepository');
const UserService = require('./userService');

class MessageService {
    getAll() {
        return MessageRepository.getAll();
    }

    create(message) {
        const user = UserService.search({ id: message.userId });
        message.user = user.name;
        message.avatar = user.avatar;
        message.likers = [];
        const newMessage = MessageRepository.create(message);
        if (!newMessage) {
            throw Error('Unnable to create message');
        }
        return newMessage;
    }

    delete(id) {
        const messageToDelete = MessageRepository.delete(id);
        if (!messageToDelete) {
            throw Error('Cannot delete this message');
        }
        return messageToDelete;
    }

    search(search) {
        const item = MessageRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const message = MessageRepository.getOne({ id });
        let updatedMessage;
        if (message) {
            message.text = dataToUpdate.text;
            updatedMessage = MessageRepository.update(id, message);
        } else {
            throw Error('Unnable to update this message');
        }
        return updatedMessage;
    }

    addLike(id, likerId) {
        const message = MessageRepository.getOne({ id });
        let updatedMessage;
        if (message) {
            if (!message.likers.includes(likerId)) {
                message.likers.push(likerId);
                updatedMessage = MessageRepository.update(id, message);
            } else {
                updatedMessage = message;
            }
        }
        if (!updatedMessage) {
            throw Error('Unnable to update this message');
        }
        return updatedMessage;
    }

    removeLike(id, likerId) {
        const message = MessageRepository.getOne({ id });
        let updatedMessage;
        if (message) {
            if (message.likers.includes(likerId)) {
                message.likers.splice(message.likers.indexOf(likerId), 1);
                updatedMessage = MessageRepository.update(id, message);
            } else {
                updatedMessage = message;
            }
        }
        if (!updatedMessage) {
            throw Error('Unnable to update this message');
        }
        return updatedMessage;
    }
}

module.exports = new MessageService();
