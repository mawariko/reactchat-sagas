import { get, post, put, deleteReq } from '../requestHelper';

export const getMessages = async () => {
  return await get('messages');
};

export const getMessage = async (id) => {
  return await get('messages', id);
};

export const createMessage = async (body) => {
  return await post('messages', body);
};

export const updateMessage = async (id, body) => {
  return await put('messages', id, body);
};

export const deleteMessage = async (id) => {
  return await deleteReq('messages', id);
};

export const addLike = async (id, likerId) => {
  return await post(`messages/${id}/likes`, { likerId });
};

export const removeLike = async (id, likerId) => {
  return await deleteReq(`messages/${id}/likes`, likerId);
};
