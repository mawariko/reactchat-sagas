import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

function Auth({ allowRoles, isLoggedIn, userRole }) {
  let redirect = null;
  if (!isLoggedIn) {
    redirect = '/';
  } else if (allowRoles.length && !allowRoles.includes(userRole)) {
    redirect = '/chat';
  }

  return redirect ? <Redirect to={redirect} /> : null;
}

Auth.defaultProps = {
  allowRoles: [],
};

const mapStateToProps = (state) => ({
  isLoggedIn: state.isLoggedIn,
  userRole: state.userRole,
});

export default connect(mapStateToProps)(Auth);
