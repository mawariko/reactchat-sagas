import React from 'react';
import Auth from '../Auth/Auth';
import userRoles from '../../constants/userRoles';

export default function Admin() {
  return (
    <>
      <Auth allowRoles={[userRoles.ADMIN]} />
      <div>This is admin's page</div>
    </>
  );
}
