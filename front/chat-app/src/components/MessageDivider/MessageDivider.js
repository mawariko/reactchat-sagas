import React from 'react';
import moment from 'moment';
import './MessageDivider.css'

export default function MessageDivider({ date }) {
    let dateString = date.format('DD MMMM');
    if (date.isAfter(moment().startOf('day'))) {
        dateString = 'Today';
    } else if (date.isAfter(moment().subtract(1, 'days').startOf('day'))) {
        dateString = 'Yesterday';
    }

    return (
        <>
        <div className='message-divider'>
           <p>{dateString}</p>
        </div>
        <div className='line-through'></div>
        </>
    )
}
