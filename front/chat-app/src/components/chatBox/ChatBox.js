import React from 'react'
import { connect } from 'react-redux'
import ChatHeader from './../ChatHeader/ChatHeader'
import MessageList from './../MessageList/MessageList'

import './ChatBox.css'

function ChatBox({ chatItems }) {
  const userNames = new Set(chatItems.map((user) => user.user))
  const lastMessageDate = chatItems.length ? chatItems[chatItems.length - 1].createdAt : null

  return (
    <div className="chat-box">
      <ChatHeader
        uniqueUsers={userNames.size}
        messageNumber={chatItems.length}
        lastMessageDate={lastMessageDate}
      />
      <MessageList />
    </div>
  )
}

const mapStateToProps = (state) => ({
  chatItems: state.chatItems,
})

export default connect(mapStateToProps)(ChatBox)