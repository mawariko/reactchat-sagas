import React, { useState } from 'react';
import { Container, Form, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { login } from '../../actions/';
import './LogIn.css';

const LogIn = ({ login }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const onPasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const onSubmit = () => {
    login(email, password);
  };

  return (
    <Container className="login-container">
      <Form size="huge">
        <Form.Field>
          <label>Email</label>
          <input
            width={8}
            placeholder="Email"
            onChange={onEmailChange}
            value={email}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input
            placeholder="Password"
            onChange={onPasswordChange}
            value={password}
          />
        </Form.Field>
        <Button
          size="huge"
          color="purple"
          className="login-submit-btn"
          type="submit"
          onClick={onSubmit}
        >
          Submit
        </Button>
      </Form>
    </Container>
  );
};

const mapDispatchToProps = {
  login,
};
export default connect(null, mapDispatchToProps)(LogIn);
