import {
  SHOW_SPINNER,
  HIDE_SPINNER,
  TOGGLE_LIKE,
  TOGGLE_LIKE_SUCCESS,
  CREATE_MESSAGE,
  CREATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  EDIT_MESSAGE,
  EDIT_MESSAGE_SUCCESS,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  CANCEL_EDIT_MESSAGE,
  CANCEL_EDIT_MESSAGE_SUCCESS,
  LOGIN,
  LOGIN_SUCCESS,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
} from './actionTypes';

export const showSpinner = (id) => ({
  type: SHOW_SPINNER,
});

export const hideSpinner = (id) => ({
  type: HIDE_SPINNER,
});

export const toggleLike = (id) => ({
  type: TOGGLE_LIKE,
  id,
});

export const toggleLikeSuccess = (id, likers, isMyLike) => ({
  type: TOGGLE_LIKE_SUCCESS,
  payload: { id, likers, isMyLike },
});

export const fetchMessages = () => ({
  type: FETCH_MESSAGES,
});

export const fetchMessagesSuccess = (messages) => ({
  type: FETCH_MESSAGES_SUCCESS,
  messages,
});

export const createMessage = (text) => ({
  type: CREATE_MESSAGE,
  text,
});

export const createMessageSuccess = (message) => ({
  type: CREATE_MESSAGE_SUCCESS,
  message,
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  id,
});

export const deleteMessageSuccess = (id) => ({
  type: DELETE_MESSAGE_SUCCESS,
  id,
});

export const editMessage = (id) => ({
  type: EDIT_MESSAGE,
  id,
});

export const editMessageSuccess = (message) => ({
  type: EDIT_MESSAGE_SUCCESS,
  message,
});

export const updateMessage = (message) => ({
  type: UPDATE_MESSAGE,
  message,
});

export const updateMessageSuccess = (message) => ({
  type: UPDATE_MESSAGE_SUCCESS,
  message,
});

export const cancelEditMessage = () => ({
  type: CANCEL_EDIT_MESSAGE,
});

export const cancelEditMessageSuccess = () => ({
  type: CANCEL_EDIT_MESSAGE_SUCCESS,
});

export const login = (email, password) => ({
  type: LOGIN,
  payload: { email, password },
});

export const loginSuccess = (isLoggedIn, userId, userRole) => ({
  type: LOGIN_SUCCESS,
  payload: { isLoggedIn, userId, userRole },
});
