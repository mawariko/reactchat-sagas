import moment from 'moment';

export default class MessageModel {
  constructor(data, myId) {
    this.id = data.id;
    this.avatar = data.avatar;
    this.user = data.user;
    this.userId = data.userId;
    this.text = data.text;
    this.createdAt = moment(data.createdAt);
    this.updatedAt = data.updatedAt ? moment(data.updatedAt) : null;
    this.isMine = data.userId === myId;
    this.likers = data.likers;
    this.isMyLike = data.likers.includes(myId);
  }
}
