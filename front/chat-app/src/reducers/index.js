import {
  SHOW_SPINNER,
  HIDE_SPINNER,
  FETCH_MESSAGES_SUCCESS,
  TOGGLE_LIKE_SUCCESS,
  CREATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_SUCCESS,
  EDIT_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_SUCCESS,
  CANCEL_EDIT_MESSAGE_SUCCESS,
  LOGIN_SUCCESS,
} from './../actions/actionTypes';
import moment from 'moment';

const initialState = {
  isLoggedIn: false,
  userId: null,
  userRole: null,
  chatItems: [],
  showSpinner: false,
  currentlyEditedMessage: null,
};

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_SPINNER:
      return {
        ...state,
        showSpinner: true,
      };
    case HIDE_SPINNER:
      return {
        ...state,
        showSpinner: false,
      };
    case FETCH_MESSAGES_SUCCESS:
      return {
        ...state,
        chatItems: action.messages,
        showSpinner: false,
      };
    case TOGGLE_LIKE_SUCCESS:
      const likedChatItems = state.chatItems.map((item) => {
        if (item.id === action.payload.id) {
          item.likers = action.payload.likers;
          item.isMyLike = action.payload.isMyLike;
        }
        return item;
      });
      return {
        ...state,
        chatItems: likedChatItems,
      };
    case CREATE_MESSAGE_SUCCESS:
      const chatItems = [...state.chatItems];
      chatItems.push(action.message);

      return {
        ...state,
        chatItems,
      };
    case DELETE_MESSAGE_SUCCESS:
      return {
        ...state,
        chatItems: state.chatItems.filter((item) => item.id !== action.id),
      };
    case EDIT_MESSAGE_SUCCESS:
      return {
        ...state,
        currentlyEditedMessage: action.message,
      };
    case UPDATE_MESSAGE_SUCCESS:
      const updatedChatItems = state.chatItems.map((item) => {
        if (item.id === action.message.id) {
          item.text = action.message.text;
          item.updatedAt = moment(action.message.updatedAt);
        }
        return item;
      });

      return {
        ...state,
        chatItems: updatedChatItems,
        currentlyEditedMessage: null,
      };
    case CANCEL_EDIT_MESSAGE_SUCCESS:
      return {
        ...state,
        currentlyEditedMessage: null,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: action.payload.isLoggedIn,
        userId: action.payload.userId,
        userRole: action.payload.userRole,
      };
    default:
      return state;
  }
}
