import React from 'react';
import { connect } from 'react-redux';
import { fetchMessages, editMessage } from '../../actions';
import './Chat.css';
import Auth from '../../components/Auth/Auth';
import ChatBox from './../../components/chatBox/ChatBox';
import SiteHeader from './../../components/SiteHeader/SiteHeader';
import MessageInput from './../../components/MessageInput/MessageInput';
import Footer from './../../components/Footer/Footer';

class Chat extends React.Component {
  componentDidMount() {
    this.props.fetchMessages();

    window.addEventListener('keydown', (e) => {
      if (e.keyCode == 38) {
        const myMessages = this.props.chatItems.filter((item) => item.isMine);
        if (myMessages.length) {
          const myLastMessage = myMessages[myMessages.length - 1];
          this.props.editMessage(myLastMessage.id, myLastMessage.text);
        }
      }
    });
  }

  render() {
    return (
      <>
        <Auth />
        <div className="content-container">
          <SiteHeader></SiteHeader>
          <ChatBox />
          <MessageInput />
          <Footer></Footer>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  chatItems: state.chatItems,
  userRole: state.userRole,
  isLoggedIn: state.isLoggedIn,
});

const mapDispatchToProps = {
  fetchMessages,
  editMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
