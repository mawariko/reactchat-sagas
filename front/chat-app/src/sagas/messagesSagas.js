import { all, takeEvery, call, put, select } from 'redux-saga/effects';
import history from './../history';
import {
  getMessages,
  getMessage,
  createMessage as serviceCreateMessage,
  deleteMessage as serviceDeleteMessage,
  updateMessage as serviceUpdateMessage,
  addLike,
  removeLike,
} from '../services/domainRequest/message';
import {
  FETCH_MESSAGES,
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  UPDATE_MESSAGE,
  CANCEL_EDIT_MESSAGE,
  TOGGLE_LIKE,
} from '../actions/actionTypes';
import {
  showSpinner,
  hideSpinner,
  fetchMessagesSuccess,
  createMessageSuccess,
  deleteMessageSuccess,
  editMessageSuccess,
  updateMessageSuccess,
  cancelEditMessageSuccess,
  toggleLikeSuccess,
} from '../actions';
import MessageModel from '../models/MessageModel';

export default function* messagesSagas() {
  yield all([
    watchFetchMessages(),
    watchCreateMessage(),
    watchDeleteMessage(),
    watchToggleLike(),
    watchEditMessage(),
    watchUpdateMessage(),
    watchCancelEditMessage(),
  ]);
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

function* watchCreateMessage() {
  yield takeEvery(CREATE_MESSAGE, createMessage);
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

function* watchToggleLike() {
  yield takeEvery(TOGGLE_LIKE, toggleLike);
}

function* watchEditMessage() {
  yield takeEvery(EDIT_MESSAGE, editMessage);
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

function* watchCancelEditMessage() {
  yield takeEvery(CANCEL_EDIT_MESSAGE, cancelEditMessage);
}

function* fetchMessages() {
  yield put(showSpinner());
  try {
    const userId = yield select((state) => state.userId);
    const messages = yield call(getMessages);
    const chatItems = messages
      .map((item) => new MessageModel(item, userId))
      .sort((a, b) => a.createdAt - b.createdAt);
    yield put(fetchMessagesSuccess(chatItems));
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}

function* createMessage(action) {
  yield put(showSpinner());
  try {
    const userId = yield select((state) => state.userId);
    const message = yield call(serviceCreateMessage, { userId, text: action.text });
    const chatItem = new MessageModel(message, userId);
    yield put(createMessageSuccess(chatItem));
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}

function* deleteMessage(action) {
  yield put(showSpinner());
  try {
    yield call(serviceDeleteMessage, action.id);
    yield put(deleteMessageSuccess(action.id));
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}

function* toggleLike(action) {
  yield put(showSpinner());
  try {
    const userId = yield select((state) => state.userId);
    const chatItem = yield select((state) =>
      state.chatItems.find((item) => item.id === action.id)
    );
    let updatedMessage;
    if (chatItem.likers.includes(userId)) {
      updatedMessage = yield call(removeLike, chatItem.id, userId);
    } else {
      updatedMessage = yield call(addLike, chatItem.id, userId);
    }
    yield put(
      toggleLikeSuccess(chatItem.id, updatedMessage.likers, !chatItem.isMyLike)
    );
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}

function* editMessage(action) {
  yield put(showSpinner());
  try {
    const message = yield call(getMessage, action.id);
    yield put(editMessageSuccess(message));
    yield call([history, history.push], '/chat/edit');
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}

function* updateMessage(action) {
  yield put(showSpinner());
  try {
    const message = yield call(serviceUpdateMessage, action.message.id, {
      id: action.message.id,
      text: action.message.text,
    });
    yield put(updateMessageSuccess(message));
    yield call([history, history.push], '/chat');
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}

function* cancelEditMessage(action) {
  yield call([history, history.push], '/chat');
  yield put(cancelEditMessageSuccess());
}
